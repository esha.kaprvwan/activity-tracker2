import { NextFunction, Router, Request, Response } from "express";
import { ROLES } from "../../utility/DB_constants";
// import { multerMiddlerWare } from "../../../utility/fileStorageEngine";
import { permit } from "../../utility/authorize";
import { ResponseHandler } from "../../utility/response";
import userServices from "./user.services";
import { CreateUserValidator, LoginUserValidator, ResetPasswordValidator } from "./user.validation";

const router = Router();

// register user
router.post("/register",
    CreateUserValidator,
    async (req: Request, res: Response, next: NextFunction) => {
        try {
            const result = await userServices.createUser(req.body);
            //send id
            const { id } = result;
            const cycle = await userServices.createCycle(id)
            res.send(new ResponseHandler(cycle));
        }
        catch (e) {
            next(e);
        }
    });

// login user
router.post("/login",
    LoginUserValidator,
    async (req: Request, res: Response, next: NextFunction) => {
        try {
            const result = await userServices.login(req.body);
            res.send(new ResponseHandler(result));
        } catch (e) {
            next(e);
        }
    });

router.post("/forgot-password",
    async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { email } = req.body;
            const result = await userServices.forgotPassword(email);
            res.send(new ResponseHandler(result));
        }
        catch (e) {
            next(e);
        }
    });

router.post("/reset-password",
    ResetPasswordValidator,
    async (req: Request, res: Response, next: NextFunction) => {
        try {
            const result = await userServices.resetPassword(req.body);
            res.send(new ResponseHandler(result));
        }
        catch (e) {
            next(e);
        }
    });

export default router;
