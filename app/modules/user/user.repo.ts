import mongoose from "mongoose";
import { ICredentials } from "../../utility/credentials";
import UserModel from "./user.schema";
import { ICycle, IUser } from "./user.types"
import { Types } from "mongoose"
import { IPass } from "./user.types";

const createUser = (user: IUser) => UserModel.create(user);
const findOne = (credentials: ICredentials) => UserModel.findOne(credentials);
const getAll = () => UserModel.find();
const getOne = (id: string) => UserModel.findById(id);
const deleteUser = (id: string) => UserModel.deleteOne({ _id: id });
//password realted operations

const findByToken = (resetToken: string) => UserModel.findOne({ resetToken: resetToken });
const resetPassword = (passwords: IPass) => UserModel.updateOne({ _id: passwords._id }, { $set: { password: passwords.confirmPassword } });
const forgotPassword = (email: string) => UserModel.findOne({ email: email });


// date related operations
const getDate = (_id: string) => UserModel.aggregate([
    {
        $match: { _id: new mongoose.Types.ObjectId(_id) }
    },
    {
        $project: {
            _id: 0,
            dateParts: { $dateToParts: { date: "$cycleFrom" } }
        }
    }
]);
const updateCycle=(id:string , cycle: ICycle[])=> UserModel.updateOne( { _id: id},{$set : { cycle : cycle}});
export default {
    createUser,
    findOne,
    getAll,
    getOne,
    deleteUser,
    findByToken,
    resetPassword,
    forgotPassword,
    getDate,
    updateCycle
};