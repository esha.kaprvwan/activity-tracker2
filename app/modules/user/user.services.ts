import { ICredentials } from "../../utility/credentials";
import userRepo from "./user.repo";
import { IUser } from "./user.types";
import { sign } from "jsonwebtoken";
import { randomBytes } from "crypto";
import { sendMail } from "../../utility/sendmail";
import { IPass } from "./user.types";
const createUser = (user: IUser) => userRepo.createUser(user);
const findOne = (credentials: ICredentials) => userRepo.findOne(credentials);
const getAll = () => userRepo.getAll();
const getOne = (id: string) => userRepo.getOne(id);
const deleteUser = (id: string) => userRepo.deleteUser(id);

const login = async (credentials: ICredentials) => {
    const userRecord = await userRepo.findOne(credentials);
    if (!userRecord) {
        throw { statusCode: 404, message: "INVALID CREDENTIALS" }
    }
    const { SECRET_KEY } = process.env;
    const token = sign(JSON.parse(JSON.stringify(userRecord)), SECRET_KEY || '');
    const { roleId } = userRecord
    return { token, roleId };
}

const resetPassword = async (passwords: IPass) => {
    if (passwords.confirmPassword === passwords.password) {
        const user = await userRepo.findByToken(passwords.resetToken);
        if (!user || user.resetToken && (passwords.resetToken !== user.resetToken) || user.resetTokenExpiry && (Date.now() >= user.resetTokenExpiry)) {
            throw { statusCode: 400, message: 'Token invalid for resetting your password' }
        }
        user.password = passwords.confirmPassword;
        user.resetToken = null;
        user.resetTokenExpiry = null;
        await user.save();
    } else {
        throw { message: "password not Matched" };
    }
    return "password successfully changed";
};

const forgotPassword = async (email: string) => {
    try {
        const data = await userRepo.forgotPassword(email);
        if (!data) throw { statusCode: 400, message: "user not found" };
        if (
            data.resetToken &&
            data.resetTokenExpiry &&
            Date.now() <= data.resetTokenExpiry
        ) throw { statusCode: 400, message: "Email for reset password already been sent" }
        data.resetToken = randomBytes(6).toString('hex');
        data.resetTokenExpiry = Date.now() + (1000 * 60 * 60);
        const result = await data.save();
        if (result._id && result.resetToken) {
            await sendMail(email, result.resetToken);
            return "Email send";
        }
    }
    catch (e) {
        throw e;
    }
};

const createCycle = async (id: string) => {
    try {
        const data = await userRepo.getDate(id);
        const { year } = data[0].dateParts;
        const { month } = data[0].dateParts;
        const { day } = data[0].dateParts;
        const firstCycle = {
            from: new Date(year, month, day),
            to: new Date(year + 2, month, year),
            procedural: [
                {
                    task:"62613799b7e4de8494df55c9",
                    optional: true
                },
                {
                    task:"626137a2b7e4de8494df55cb"
                },
                {
                    task:"626137a7b7e4de8494df55cd"
                }
               
            ],
            clinical: [
                {
                    task:"626137abb7e4de8494df55cf",
                 
                },
                {
                    task:"626137afb7e4de8494df55d1"
                },
                {
                    task:"626137b2b7e4de8494df55d3"
                }

            ],
            document: [
                {
                    task:"626137b6b7e4de8494df55d5",
                 
                },
                {
                    task:"626137bab7e4de8494df55d7"
                },
                {
                    task:"626137beb7e4de8494df55d9"
                }
                
            ]
        };
        const SecondCycle = {
            from: new Date(year + 2, month, day),
            to: new Date(year + 4, month, year),
            procedural: [
                {
                    task:"626137c2b7e4de8494df55db",
                    // optional: true
                },
                {
                    task:"626137c5b7e4de8494df55dd"
                },
                {
                    task:"626137c8b7e4de8494df55df"
                }
               
            ],
            clinical: [
                {
                    task:"626137ccb7e4de8494df55e1",
                 
                },
                {
                    task:"626137d0b7e4de8494df55e3"
                },
                {
                    task:"626137d5b7e4de8494df55e5"
                }

            ],
            documental: [
                {
                    task:"626137d9b7e4de8494df55e7",
                 
                },
                {
                    task:"626137ddb7e4de8494df55e9"
                },
                {
                    task:"626137e1b7e4de8494df55eb"
                }
                
            ]
        };
        const thirdCycle = {
            from: new Date(year + 4, month, day),
            to: new Date(year + 6, month, year),
            procedural: [
                {
                    task:"626137e5b7e4de8494df55ed",
                    // optional: true
                },
                {
                    task:"626137eab7e4de8494df55ef"
                },
                {
                    task:"626137edb7e4de8494df55f1"
                }
               
            ],
            clinical: [
                {
                    task:"626137f0b7e4de8494df55f3",
                 
                },
                {
                    task:"626137f3b7e4de8494df55f5"
                },
                {
                    task:"626137f7b7e4de8494df55f7"
                }

            ],
            documental: [
                {
                    task:"626137fcb7e4de8494df55f9",
                 
                },
                {
                    task:"626137ffb7e4de8494df55fb"
                },
                {
                    task:"62613802b7e4de8494df55fd",
                    optional:true
                }
                
            ]
        };
        const cycle = [firstCycle, SecondCycle, thirdCycle];
        const userData = await userRepo.updateCycle(id,cycle);
        return userData;

    }
    catch (e) {
        throw e;
    }
}

export default {
    createUser,
    findOne,
    getAll,
    getOne,
    login,
    deleteUser,
    resetPassword,
    forgotPassword,
    createCycle
};