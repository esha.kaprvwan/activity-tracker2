export interface IPass {
    _id?: string,
    resetToken: string,
    password: string,
    confirmPassword: string
}
export interface ICycle {
    _id?: string,
    from: Date,
    to: Date,
    procedural: [ITask],
    clinical: [ITask],
    document: [ITask]
}
export interface ITask{
    task: string,
    filename:string,
    path: string,
    taskStatus:string,
    optional:Boolean

}
export interface IUser {
    _id?: string,
    name: string,
    email: string,
    password: string,
    roleId: string,
    resetToken?: string | null,
    resetTokenExpiry?: number | null,
    userStatus: string,
    cycleFrom: Date,
    cycleTo: Date,
    cycle: [Object]
}